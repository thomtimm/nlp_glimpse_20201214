# Natural Language Processing - a glimpse into theory and practice

This repository contains the notebooks and handout for the following presentation:

title
: Natural Language Processing - a glimpse into theory and practice

author
: Thomas Timmermann

date
: 2020-12-14

occasion
: lecture "Data Science in Context", TU Dortmund


## Installation

You need python to run the notebook. The following instructions assume you're on linux. If you're on a windows machine, consider switching to the [windows subsystem for linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

If you have `make`, just run

``` shsh
make install
```

Otherwise,  do the following:

1. Create a virtual environment with

``` sh
python3 -m venv .vev
```

2. Activate the environment and install dependencies listed in `requirements.txt`:

``` sh
source .venv/bin/activate && pip install -r requirements.txt
```

In case the pinned versions of the dependencies fail to install, you can try without pinned versions:


``` sh
source .venv/bin/activate && pip install -r requirements.in
```

3. Install the language models to use the word vectors:

``` sh
source .venv/bin/activate && python -m spacy download de_core_news_md
source .venv/bin/activate && python -m spacy download en_core_web_md
```

## Usage

Activate the virtual environment and start [jupyter](https://jupyter.org) or [jupyter lab](https://jupyter.org) to open the notebook:

``` sh
source .venv/bin/activate && jupyter lab ner_legal_keras.ipynb
```
