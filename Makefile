.PHONY: install jupyter

PYTHON=python3
ACTIVATE_VENV = source .venv/bin/activate

.venv:
	$(PYTHON) -m venv .venv && $(ACTIVATE_VENV) && pip install --upgrade pip

install: .venv
	$(ACTIVATE_VENV) && pip install -r requirements.txt
	$(ACTIVATE_VENV) && python -m spacy download de_core_news_md
	$(ACTIVATE_VENV) && python -m spacy download en_core_web_md


jupyter:
	$(ACTIVATE_VENV) && jupyter lab .
